% =========================================================================
% OVERVIEW : State graphs. Forward chaining. Water jugs problem
% =========================================================================

:- include('stateGraph.pl'). 

%=======================================================================
% --- problem name: jugs(CapacityA, CapacityB, WantedLiters)
%
% --- state: jugs(contents(A,B), capacity(CapA,CapB), target(Wanted))
%
% --- transitions: empty(a), empty(b), fill(a), fill(b), 
%                  transfer(a,b), transfer(b,a)

initialState(jugs(A,B,T), S) :-
    S = jugs(contents(0,0), capacity(A,B), target(T)).
    
finalState(jugs(contents(T,_), _, target(T))).
finalState(jugs(contents(_,T), _, target(T))).

% it will be checked in the transitions...
legalState(jugs(_,_,_)).  

% TODO - replace the ______________ with something reasonable: 

transition(jugs(contents(_,Vb), C, T),    empty(a), 
           jugs(______________,__,__)).

% TODO - A COMPLETER...

%----------------------------
go(M) :-
    Goal=solveStateProblem(jugs(8,5,7), M),
    write(Goal), nl,
    Goal.

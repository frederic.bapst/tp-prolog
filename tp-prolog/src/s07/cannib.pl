% =========================================================================
% OVERVIEW : State graphs. Forward chaining. Missionaries & cannibals problem
% =========================================================================

:- include('stateGraph.pl').  

%=======================================================================
% --- problem name: cannibals(N, BoatCapacity)
%                   for N missionaries + N cannibals, 
%                   and a boat for [1..BoatCapacity] persons
% --- state: cannib(BoatPosition, left(M,C),  right(M,C), capacity(Cap) )
%                   BoatPosition: left/right
%
% --- transitions: move(nbOfMission, nbOfCannib)

initialState(cannibals(N, Cap), cannib(left, left(N,N), right(0,0), capacity(Cap))).

%  TODO - replace _____________ with something reasonable: 

finalState(_____________).

% TODO - A COMPLETER...

%=======================================================================
%--- nbBetween(+From, +To, ?N) :- N is an int between From and To
nbBetween(From, _To, From).
nbBetween(From, To, N) :- 
    From < To,
    From1 is From+1,
    nbBetween(From1, To, N).


go(M) :-
    Goal=solveStateProblem(cannibals(3, 2), M),
    write(Goal), nl,
    Goal.

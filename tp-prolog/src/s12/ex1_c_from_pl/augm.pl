%--- Declarations for the "compiler" gplc
%--- (not meaningful for the interpreter gprolog)

:- foreign(first_occurrence(+string,+char,-positive)).
:- foreign(char_ascii(?char,?code)).  

%--- Note: each argument must correspond to a supported type

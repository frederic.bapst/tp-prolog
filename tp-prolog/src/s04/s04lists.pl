% =========================================================================
% FILE     : s04lists.pl
% SUPPORT  : Bapst Frederic, HEIA-FR.
% CONTEXT  : Programmation Logique
% =========================================================================
% OVERVIEW : Lists in Prolog, accumulators
% =========================================================================

% ------------------------------------------------------------
%--- factorial(+N, ?F) : F is the factorial of N

factorial(0, 1).
factorial(N, F) :- 	
        N>0, 
        N1 is N-1, 
        factorial(N1, F1),
        F is F1 * N.

% TODO - A COMPLETER


% ------------------------------------------------------------
%--- listSum(+Ls, ?S) : Ls is a list of ints, S is the sum

% TODO - A COMPLETER


% ------------------------------------------------------------
% --- concat(?As, ?Bs, ?Cs, ?AsBsCs) : 
%        AsBsCs is the concatenation of As, Bs and Cs
% ---
concat1(As, Bs, Cs, Ls) :-
    append(As, Bs, Xs),
    append(Xs, Cs, Ls).
% ---
concat2(As, Bs, Cs, Ls) :-
    append(Xs, Cs, Ls),
    append(As, Bs, Xs).
% ---
concat3(As,Bs,Cs,Ls) :- var(Ls),
    concat1(As,Bs,Cs,Ls).
concat3(As,Bs,Cs,Ls) :- nonvar(Ls),
    concat2(As,Bs,Cs,Ls).

% ------------------------------------------------------------
% --- Ex. 4
% |?- countAB('t.txt').
%   le fichier t.txt contient 3 a et 25 b
% |?- countAB('noFile.zzz').
%   Oups, erreur I/O
% 
% Please verify the behavior of your code (there should be only _one_ solution!)

% TODO - A COMPLETER
    
% ------------------------------------------------------------
% --- p(+Ls, ?Rs): ...
p(Ls, [E|Rs]) :-
    append(As, [E|Bs], Ls),
    append(As, Bs, Xs),
    p(Xs, Rs).
p(Ls, Ls).

    
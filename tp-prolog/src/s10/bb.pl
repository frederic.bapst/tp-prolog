% =========================================================================
% FILE     : bb.pl
% SUPPORT  : Bapst Frederic, HEIA-FR.
% CONTEXT  : Programmation Logique
% =========================================================================
% OVERVIEW : Blackboard engine
% =========================================================================

:-  op(650, xfx, '~~>').

%---------------------------------------------------------
%--- bb_run(+InitialBlackboard, -FinalBlackboard)
%---------------------------------------------------------
% bb_run(BB, BBFinal) :- ...


%--- bb_applySome(+BB, +PossibleRules, -BBFinal) : 
%      BBFinal is the result when applying a rule randomly taken
%      from the list, then running the rest of the program

bb_applySome(BB, [], BB). % no more possible rules, exit.
% bb_applySome(BB, Rules, BBFinal) :-  ...

%--- bb_oneRule(+BB, -R) : R is a rule with IN data in blackboard,
%                          and valid Conditions
% bb_oneRule(BB, In/Cond~~>Body) :- ...

%--- bb_filter(+BB, -Rs) : Rs contains every matching rule with respect to BB
% bb_filter(BB, Rs) :- ...

%--- bb_solve(+GoalList) : solves each goal in the list
% ...

%---bb_applyRule(+BB, +Rule, -NewBB):  
%     NewBB is the blackboard after removing IN data from BB,
%     playing the actions, and adding OUT data into BB
% bb_applyRule(BB, In/_Cond~~>Action/Out, NewBB1) :-  ...

%--- bb_extract(+BB, +InList, -NewBB) : NewBB is BB without In elements
%--- Example: bb_extract([a,b,c,d,e,f], [b,e], R) gives R = [a,c,d,f]
% ...

%---bb_db(+BB) : current blackboard tracing
bb_db(BB) :- write(blackboard(BB)), nl.
% bb_db(BB) :- write(blackboard(BB)), nl, get_char(_).


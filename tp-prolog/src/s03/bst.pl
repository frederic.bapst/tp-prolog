% =========================================================================
% FILE     : bst.pl
% SUPPORT  : Bapst Frederic, HEIA-FR.
% CONTEXT  : Programmation Logique
% =========================================================================
% OVERVIEW : Abstract Data Types in Prolog - Binary Search Trees
% =========================================================================

% ------------------------------------------------------------
% --- Test programs
% ------------------------------------------------------------
adt_test_apply(_, Adt, [], Adt).
adt_test_apply(G, Adt, [C|Cmds], AdtEnd) :-
        Goal =.. [G, Adt, C, Adt1],
        write(trying(Goal)), nl,
        Goal,
        write(done(Goal)), nl,
        adt_test_apply(G, Adt1, Cmds, AdtEnd).
% ------------------------------------------------------------
bst_test :-
        bst_new(Bst),
        Cmds = [put(10,1), 
                put(20,2), 
                put(15,9),
                get(10,A), 
                put(30,3),
                put(25,8), 
                remove(40),
                remove(20), 
                get(30,B),
                get(15,C),
                get(25,D)],
        adt_test_apply(bst_apply, Bst, Cmds, Bst1),
     \+ bst_apply(Bst1, get(40,_),_),
     \+ bst_apply(Bst1, get(20,_),_),
        assertEquals([1,3,9,8], [A,B,C,D]),
        write('Test passed successfully.').

assertEquals(Expected, Effective) :-
	Expected==Effective.
assertEquals(Expected, Effective) :-
	Expected\==Effective,
	write('bad news... Expected: '), write(Expected),
	write(' Effective: '), write(Effective), nl, 
	fail.



% ------------------------------------------------------------
% bst : Map (dictionary) ADT (with numbers as keys)
%    Operations :
%      new(-X),      put(+Key, +Value),
%      remove(+Key), get(+Key, -Value),  
%    Representation : with binary search tree
%      nil, or n(Key, Value, LeftBST, RightBST)

bst_new(nil).

bst_apply(n(K,_,L,R),   put(K,Val), n(K,Val,L,R)).
bst_apply(nil,          put(K,Val), n(K,Val,nil,nil)).
bst_apply(n(K1,V,L,R),  put(K,Val), n(K1,V,L1,R)) :- K < K1,
        bst_apply(L, put(K,Val), L1).
% bst_apply(???,          put(K,Val), ???         ) :- K > K1,
%         ???

% --- bst_apply(???,   remove(K), ???)...
%    5 rules :
%       - empty tree
%       - reached node without right child
%       - reached node with right child (needs left rotation)
%       - current node is smaller
%       - current node is greater

% --- bst_apply(???,   get(K,Val), ???)...


%=======================================================================
%   Sentence ::= NounPhrase VerbPhrase
% VerbPhrase ::= Verb [NounPhrase]
% NounPhrase ::= [Determiner] AdjNoun
%    AdjNoun ::= {Adjective} Noun
% Determiner ::= 'the'|'a'|...
%       Noun ::= 'cat'|'cats'|'mouse'|'mice'|...
%       Verb ::= 'eat'|'eats'|...
%  Adjective ::= 'red'|'pretty'|...

% TODO - A ADAPTER

determiner( _Num)    --> [W], {member(W, [the, my])}.
determiner(singular) --> [W], {member(W, [a, this])}.
determiner(plural)   --> [W], {member(W, [those, many])}.

noun(singular)       --> [W], {member(W, [cat, mouse, dog])}.
noun(plural)         --> [W], {member(W, [cats, mice, dogs])}.

verb(plural)         --> [W], {member(W, [eat,  follow ])}.
verb(singular)       --> [W], {member(W, [eats, follows])}.

adjective            --> [W], {member(W, [red, pretty, old])}.

sentence --> nounPhrase(Num), verbPhrase(Num).

nounPhrase(Num) --> determiner(Num), noun(Num).
nounPhrase(Num) --> noun(Num).

verbPhrase(Num) --> verb(Num).
verbPhrase(Num) --> verb(Num), nounPhrase(_Num1).

%=======================================================================
ex1_test :-
        write('Trying some sentences...'), nl,
        sentence([the,red,cat,eats,a,mouse], []),
        sentence([the,cats,eat,a,red,mouse], []),
        sentence([red,cats,eat,pretty,mice], []),
        sentence([my,cats,and,the,dog,eat,the,pretty,mouse],[]),
        sentence([my,dog,and,the,red,cat,eat,a,pretty,mouse],[]),
        write('Trying several adjectives...'), nl,
        sentence([the,pretty,red,cat,eats,a,mouse], []),
        write('Trying a wrong sentence...'), nl,        
     \+ sentence([my,dog,and,the,red,cat,eats,a,pretty,mouse],[]),
     	write('Test OK').
        
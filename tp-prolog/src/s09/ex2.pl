%=======================================================================
%   Sentence ::= NounPhrase VerbPhrase
% VerbPhrase ::= Verb [NounPhrase]
% NounPhrase ::= [Determiner] Noun
% Determiner ::= 'the'|'a'|...
%       Noun ::= 'cat'|'cats'|'mouse'|'mice'|...
%       Verb ::= 'eat'|'eats'|...
%  Adjective ::= 'red'|'pretty'|...

% TODO - A ADAPTER

determiner( _Num)    --> [W], {member(W, [the, my])}.
determiner(singular) --> [W], {member(W, [a, this])}.
determiner(plural)   --> [W], {member(W, [those, many])}.

noun(singular)       --> [W], {member(W, [cat, mouse, dog])}.
noun(plural)         --> [W], {member(W, [cats, mice, dogs])}.

verb(plural)         --> [W], {member(W, [eat,  follow ])}.
verb(singular)       --> [W], {member(W, [eats, follows])}.

sentence --> nounPhrase(Num), verbPhrase(Num).

nounPhrase(Num) --> determiner(Num), noun(Num).
nounPhrase(Num) --> noun(Num).

verbPhrase(Num) --> verb(Num).
verbPhrase(Num) --> verb(Num), nounPhrase(_Num1).

%=======================================================================
ex2_test :-
        sentence(X, [the,cat,eats,a,mouse],[]), 
        Y = s( np(det(the),noun(cat)), vp(verb(eats), np(det(a), noun(mouse)) )),
        checkOk(X,Y),
        write('Test passed successfully'),nl.
checkOk(X,Y) :- X==Y, !.
checkOk(X,Y) :- write('Observed: '), write(X), nl, write('Expected: '), write(Y), nl, fail.


